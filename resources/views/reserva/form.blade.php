<div class="form-group">
<label for="nombre" class="control-label">{{'Nombre'}}</label>
<input type="text" class="form-control {{ $errors->has('nombre')?'is-invalid':'' }}" 
name="nombre" id="nombre" 
value="{{ isset($reserva->nombre)?$reserva->nombre:old('nombre') }}"
>
{!! $errors->first('nombre','<div class="invalid-feedback">:message</div>') !!}
</div>

<div class="form-group">
<label for="fechaini" class="control-label">{{'Fecha de inicio'}}</label>
<input type="text" class="form-control {{ $errors->has('fechaini')?'is-invalid':'' }}" 
name="fechaini" id="fechaini" 
value="{{ isset($reserva->fechaini)?$reserva->fechaini:old('fechaini') }}"
>
{!! $errors->first('fechaini','<div class="invalid-feedback">:message</div>') !!}
</div>

<div class="form-group">
<label for="fechafin" class="control-label">{{'Fecha final'}}</label>
<input type="text" class="form-control {{ $errors->has('fechafin')?'is-invalid':'' }}" 
name="fechafin" id="fechafin" 
value="{{ isset($reserva->fechafin)?$reserva->fechafin:old('fechafin') }}"
>
{!! $errors->first('fechafin','<div class="invalid-feedback">:message</div>') !!}
</div>

<div class="form-group">
<label for="ciudaden" class="control-label">{{'Ciudad de Entrega'}}</label>
<input type="text" class="form-control {{ $errors->has('ciudaden')?'is-invalid':'' }}" 
name="ciudaden" id="ciudaden" 
value="{{ isset($reserva->ciudaden)?$reserva->ciudaden:old('ciudaden') }}"
>
{!! $errors->first('ciudaden','<div class="invalid-feedback">:message</div>') !!}
</div>

<div class="form-group">
<label for="ciudaddev" class="control-label">{{'Ciudad de Devolucion'}}</label>
<input type="text" class="form-control {{ $errors->has('ciudaddev')?'is-invalid':'' }}" 
name="ciudaddev" id="ciudaddev" 
value="{{ isset($reserva->ciudaddev)?$reserva->ciudaddev:old('ciudaddev') }}"
>
{!! $errors->first('ciudaddev','<div class="invalid-feedback">:message</div>') !!}
</div>

<div class="form-group">
<label for="descripcion" class="control-label">{{'Descripcion'}}</label>
<input type="text" class="form-control {{ $errors->has('descripcion')?'is-invalid':'' }}" 
name="descripcion" id="descripcion" 
value="{{ isset($reserva->descripcion)?$reserva->descripcion:old('descripcion') }}"
>
{!! $errors->first('descripcion','<div class="invalid-feedback">:message</div>') !!}
</div>

<div class="form-group">
<label for="valor" class="control-label">{{'Valor'}}</label>
<input type="text" class="form-control {{ $errors->has('valor')?'is-invalid':'' }}" 
name="valor" id="valor" 
value="{{ isset($reserva->valor)?$reserva->valor:old('valor') }}"
>
{!! $errors->first('valor','<div class="invalid-feedback">:message</div>') !!}
</div>


<input type="submit" class="btn btn-success" value="{{ $Modo=='crear' ? 'Agregar':'Modificar'}}">
<a class="btn btn-primary" href="{{ url('reserva') }}">Regresar</a>