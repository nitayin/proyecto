@extends('layouts.app')

@section('content')

<div class="container">

@if(Session::has('Mensaje'))
<div class="alert alert-success" role="alert">
{{
    Session::get('Mensaje')
}}
</div>
@endif

<a href="{{ url('reserva/create') }}" class="btn btn-success">Agregar reserva</a>
<br/>
<br/>


<table class="table table-light table-hover">

    <thead class="thead-light">
        <tr>
            <th>#</th>
            <th>Nombre</th>
            <th>Fecha inicio</th>
            <th>Fecha fin</th>
            <th>Ciudad Entrega</th>
            <th>Ciudad Devolucion</th>
            <th>Descripcion</th>
            <th>Valor</th>
            <th>Acciones</th>
        </tr>
    </thead>

    <tbody>
    @foreach($reserva as $reserva)
        <tr>
           <td>{{$loop->iteration}}</td>
            <td>{{$reserva->nombre}}</td>
            <td>{{$reserva->fechaini}}</td>
            <td>{{$reserva->fechafin}}</td>
            <td>{{$reserva->ciudaden}}</td>
            <td>{{$reserva->ciudaddev}}</td>
            <td>{{$reserva->descripcion}}</td>
            <td>{{$reserva->valor}}</td>
            <td>

            <a class="btn btn-warning" href="{{ url('/reserva/'.$reserva->id.'/edit') }}">
            Editar
            </a>
        
            <form method="post" action="{{ url('/reserva/'.$reserva->id) }}"  style="display:inline">
            {{csrf_field() }}
            {{method_field('DELETE') }}
            <button class="btn btn-danger" type="submit" onclick="return confirm('¿Borrar?');">Borrar</button>
    
            </form>

        </tr>
     
    @endforeach
    
    </tbody>

</table>

</div>

@endsection