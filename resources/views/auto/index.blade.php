@extends('layouts.app')

@section('content')

<div class="container">

@if(Session::has('Mensaje'))
<div class="alert alert-success" role="alert">
{{
    Session::get('Mensaje')
}}
</div>
@endif

<a href="{{ url('auto/create') }}" class="btn btn-success">Agregar auto</a>
<br/>
<br/>


<table class="table table-light table-hover">

    <thead class="thead-light">
        <tr>
            <th>#</th>
            <th>Foto</th>
            <th>Placa</th>
            <th>Modelo</th>
            <th>Marca</th>
            <th>Cilindraje</th>
            <th>Estado</th>
            <th>Color</th>
            <th>Acciones</th>
        </tr>
    </thead>

    <tbody>
    @foreach($auto as $auto)
        <tr>
            <td>{{$loop->iteration}}</td>
            <td>
            <img src="{{ asset('storage').'/'.$auto->foto}}" class="img-thumbnail img-fluid" alt="" width="150">
            </td>      
            <td>{{$auto->placa}}</td>
          
            <td>{{$auto->modelo}}</td>
            <td>{{$auto->marca}}</td>
            <td>{{$auto->cilindraje}}</td>
            <td>{{$auto->estado}}</td>
            <td>{{$auto->color}}</td>
            <td>

            <a class="btn btn-warning" href="{{ url('/auto/'.$auto->id.'/edit') }}">
            Editar
            </a>
            
            
        
            <form method="post" action="{{ url('/auto/'.$auto->id) }}"  style="display:inline">
            {{csrf_field() }}
            {{method_field('DELETE') }}
            <button class="btn btn-danger" type="submit" onclick="return confirm('¿Borrar?');">Borrar</button>
    
            </form>

        </tr>
     
    @endforeach
    
    </tbody>

</table>

</div>

@endsection