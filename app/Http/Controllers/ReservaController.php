<?php

namespace App\Http\Controllers;

use App\reserva;
use Illuminate\Http\Request;

class ReservaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $dato['reserva']=reserva::paginate(5);

        return view('reserva.index',$dato);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('reserva.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $campos=[
            'nombre' => 'required|string|max:100',
            'fechaini' => 'required|string|max:100',
            'fechafin' => 'required|string|max:100',
            'ciudaden' => 'required|string|max:100',
            'ciudaddev' => 'required|string|max:100',
            'descripcion' => 'required|string|max:100',
            'valor' => 'required|string|max:100',
        ];
        $Mensaje=["required"=>'The :attribute is required'];
        $this->validate($request,$campos,$Mensaje);

        $datosreserva=request()->except('_token');

        
        reserva::insert($datosreserva);

        return redirect('reserva')->with('Mensaje','reserva agregado con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\reserva  $reserva
     * @return \Illuminate\Http\Response
     */
    public function show(reserva $reserva)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\reserva  $reserva
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $reserva= reserva::findOrfail($id);

        return view('reserva.edit',compact('reserva'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\reserva  $reserva
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $campos=[
            'nombre' => 'required|string|max:100',
            'fechaini' => 'required|string|max:100',
            'fechafin' => 'required|string|max:100',
            'ciudaden' => 'required|string|max:100',
            'ciudaddev' => 'required|string|max:100',
            'descripcion' => 'required|string|max:100',
            'valor' => 'required|string|max:100',
        ];
        
        $Mensaje=["required"=>'The :attribute is required'];
        $this->validate($request,$campos,$Mensaje);

        
        $datosreserva=request()->except(['_token','_method']);


        reserva::where('id','=',$id)->update($datosreserva);

        return redirect('reserva')->with('Mensaje','reserva modificado con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\reserva  $reserva
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $reserva= reserva::findOrfail($id);
        
        reserva::destroy($id);
    

        return redirect('reserva')->with('Mensaje','reserva eliminado con exito');
    }
}
