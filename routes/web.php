<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes(['reset'=>false]);
//'register'=>false,

Route::resource('auto', 'AutoController')->middleware('auth');
//Route::get('/reserva', 'ReservaController@index');
//Route::get('/reserva/create','ReservaController@create');
Route::resource('reserva', 'ReservaController')->middleware('auth');